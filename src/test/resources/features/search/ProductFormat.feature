Feature: Search for the product get content format type
##Positive Scenario
  Scenario Outline: Verify the response content format
    When User sent API request for the below <Products>
    Then API response content type should be JSON format
    Examples:           
      | Products  |
      | water     |
      | apple     |
      | mango     |
      | tofu      |
