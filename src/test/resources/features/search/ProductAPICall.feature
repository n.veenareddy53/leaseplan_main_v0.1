Feature: Validate retrieving the Provider API Status
@ProductAPICall-200
Scenario Outline: Verifying API with valid <Products>
When User sends an api request for <Products>
Then User Compares the actual status code with expected <StatusCode>
Examples:
| Products     | StatusCode |
| apple        | 200        |
| mango        | 200        |
| tofu         | 200        |
| water        | 200        |

@ProductAPICall-404
Scenario Outline: Verifying Products with Invalid <Products>
  When User sends an api request for <Products>
  Then User Compares the actual status code with expected <StatusCode>
Examples:
| Products    | StatusCode |
| appla       | 404        |
| Mange       | 404        |
| tafu        | 404        |
| woter       | 404        |
