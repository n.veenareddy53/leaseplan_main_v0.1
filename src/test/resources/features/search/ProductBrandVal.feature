Feature: Validate retrieving the exact brand value
  @ProductBrandValue
  Scenario Outline: Verify the products with exact brand value
    When User sent API request for the below <Products>
    Then User checks for the exact brand value
    Examples:
      | Products  |
      | apple     |
