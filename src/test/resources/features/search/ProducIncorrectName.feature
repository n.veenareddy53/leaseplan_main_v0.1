Feature: Search for the incorrect product name

##Negative Scenario 1
  Scenario Outline: Verify the API with wrong product
    When User sent API request for the below <Products>
    Then User sees error message displays and confirms status code is not ok
    Examples:           
      | Products  |
      | grapes    |
      | melons    |
      | meat      |
      | berries   |
