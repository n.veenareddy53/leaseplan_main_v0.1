package com.grocery.products.models;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2021-09-27T15:19:39.898831+02:00[Europe/Amsterdam]")
public class Product {

    public static final String SERIALIZED_NAME_BRAND = "brand";
    @SerializedName(SERIALIZED_NAME_BRAND)
    private String brand;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public static final String SERIALIZED_NAME_PROVIDER = "provider";
    @SerializedName(SERIALIZED_NAME_PROVIDER)
    private String provider;

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Product product = (Product) o;
        return Objects.equals(this.brand, product.brand) &&
                Objects.equals(this.provider, product.provider);
    }

    @Override
    public int hashCode() {
        return Objects.hash(provider, brand);
    }

    @Override
    public String toString() {
        return "Product{" +
                "brand='" + brand + '\'' +
                ", provider='" + provider + '\'' +
                '}';
    }
}
