package com.grocery.products.action;

import com.grocery.products.utility.Constant;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.apache.hc.core5.http.HttpStatus;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;

public class ProductFormatAction {
    public static Response response;

    @Step("Retrieve APIRequest")
    public static void apiRequestGetMethod(String products) throws Exception {
        response = SerenityRest.given().get(Constant.URL + products);
        System.out.println("sending request to Url:" + Constant.URL + products);
    }
    @Step("API response content type json format")
    public static void apiResponseGetContentTypeShouldJsonFormat() {
        restAssuredThat(response -> response.assertThat().contentType("application/json"));
        System.out.println("Response content type is in JSON Format");
    }
}