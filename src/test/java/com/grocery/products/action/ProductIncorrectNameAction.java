package com.grocery.products.action;
import com.grocery.products.utility.Constant;
import net.serenitybdd.rest.SerenityRest;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import org.apache.hc.core5.http.HttpStatus;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;

public class ProductIncorrectNameAction {
    public Response response;

    @Step("Retrieve APIRequest")
    public void apiRequestGetMethod(String products) throws Exception {
        response = SerenityRest.given().get(Constant.URL + products);
        System.out.println("sending request to Url:" + Constant.URL + products);
    }

/*    @Step("API response with status code")
    public void apiRequestGetResponseCode() throws Exception {
        restAssuredThat(response -> response.statusCode(HttpStatus.SC_OK));
    }

    @Step("API response success status")
    public void apiResponseGetStatusSuccess() {
        restAssuredThat(response -> response.statusCode(200));
    }

    @Step("API response content type json format")
    public void apiResponseGetContentTypeShouldJsonFormat() {
        restAssuredThat(response -> response.assertThat().contentType("application/json"));
        System.out.println("Response content type is in JSON Format");
    }
*/
    @Step("API error message displays and confirms status code is not ok")
    public void apiErrorMessageDisplayStatusCode() {
        restAssuredThat(response -> response.statusCode(HttpStatus.SC_NOT_FOUND).body("detail.message", equalTo("Not found")));
    }

}