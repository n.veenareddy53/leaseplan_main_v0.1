package com.grocery.products.action;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import com.grocery.products.utility.Constant;

public class ProductAPIAction {
    public Response response;

    @Step("Retrieve Url")
    public void requestProductWithGetMethod(String products) throws Exception {
        response = SerenityRest.given().get(Constant.URL + products);
        System.out.println("sending request to Url:" + Constant.URL + products);
    }

    @Step("Retrieve http status code")
    public int getStatusCode() throws Exception {
        return response.then().extract().statusCode();
    }

    @Step("Retrieve http content code")
    public String getContentType() throws Exception {
        return response.then().extract().contentType();
    }
}