package com.grocery.products.action;

import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.hasItems;

public class ProductBrandAction
{
    public Response response;

    @Step("API get checks for the exact brand value")
    public void apiCheckExactBrandValue()
    {
        restAssuredThat(response -> response.body("brand",hasItems("I'm Juice","Apple Bandit")));
    }
}