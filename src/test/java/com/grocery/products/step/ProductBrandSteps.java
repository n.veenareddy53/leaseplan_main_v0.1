package com.grocery.products.step;
import com.grocery.products.action.ProductBrandAction;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;

public class ProductBrandSteps
{
    private static final String HttpStats = null;
    public Response response;
    @Steps
    ProductBrandAction ProductAction= new ProductBrandAction();
    @Then("User checks for the exact brand value")
    public void user_checks_for_the_exact_brand_value()
    {
        ProductAction.apiCheckExactBrandValue();
    }
}
