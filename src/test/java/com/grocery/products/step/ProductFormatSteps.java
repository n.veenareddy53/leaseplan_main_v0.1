package com.grocery.products.step;
import com.grocery.products.action.ProductFormatAction;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;

public class ProductFormatSteps {
    private static final String HttpStats = null;
    public Response response;
    @Steps
    ProductFormatAction ProductAPI = new ProductFormatAction();

    @Then("API response content type should be JSON format")
    public void api_response_content_type_should_be_json_format() {
        ProductFormatAction.apiResponseGetContentTypeShouldJsonFormat();
    }
}
    


