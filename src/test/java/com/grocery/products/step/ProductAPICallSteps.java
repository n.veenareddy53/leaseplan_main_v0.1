package com.grocery.products.step;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import com.grocery.products.action.ProductAPIAction;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ProductAPICallSteps {

    @Steps
    ProductAPIAction ProductAPI = new ProductAPIAction();

    @When("^User sends an api request for (.*)$")
    public void user_sends_an_api_request_for_product(String Products)  throws Exception {
        ProductAPI.requestProductWithGetMethod(Products);
    }

    @Then("^User Compares the actual status code with expected (.*)$")
     public void user_compares_the_actual_status_code_with_expected(int StatusCode) throws Exception {
        assertTrue("Validation failed as content type is not json", ProductAPI.getContentType().contains("application/json"));
        assertEquals("Status Code is not as expected", StatusCode, ProductAPI.getStatusCode());
    }

}