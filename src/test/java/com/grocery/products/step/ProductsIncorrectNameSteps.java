package com.grocery.products.step;
import com.grocery.products.action.ProductIncorrectNameAction;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;

public class ProductsIncorrectNameSteps
{
    private static final String HttpStats = null;
    public Response response;
    @Steps
    ProductIncorrectNameAction ProductIncorrectName = new ProductIncorrectNameAction();
    @Given("^User sent API request for the below (.*)$")
      public void user_sent_api_request_for_the_below (String Products) throws Exception
    {
        ProductIncorrectName.apiRequestGetMethod(Products);
    }

    @Then("User sees error message displays and confirms status code is not ok")
    public void user_sees_error_message_displays_and_confirms_status_code_is_not_ok()
    {
        ProductIncorrectName.apiErrorMessageDisplayStatusCode();
    }
}
