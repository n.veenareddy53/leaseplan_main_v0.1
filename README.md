# Test Automation using java Serenity, cucumber and  maven
Open API - Test cases has been created for LeasePlan, as test assignment.

# Getting started

# Testing API's for products (mango, apple, tofu, water)
This project was done using Java Serenity, Cucumbeer BDD framework and Maven build tool

# Installing
In order to execute API tests, it is necessary to configure CI/CD pipeline with docker maven image

# How to build
Run below maven command to build the project.
$ mvn clean

# How to execute test cases [as below]
Run all the test cases.
$ mvn verify

# Write new test cases in a file with the path specified below
src/test/resources/features.search  [for writing scenerio's]
src/test/java/com/grocery/products/step/  [for calling methods]
src/test/java/com/grocery/products/action/ [for writing methods]
src/test/java/com/grocery/products/models/ [for writing models]
src/test/java/com/grocery/products/utility/Constants  [for writing constant values used in test cases as per requirement]

# Serenity HTML Reports
[Reports are generated in target/site/serenity/index.html file]

# Refactored
Added all the maven,rest-assured,serenity,cucumber,junit and required dependencies in pom.xml file which are required to make use in writing functionalities.
Correct ordering of packages and files in such a way that it's easy to understand the flow of project
Added Constants.java file under util package to define all the constants in the project

# Summary - Test Cases Coverability
* Verify the API with wrong product
* Verifying API with valid Products
* Verify the exact brand value
* Verify the response content format

# Complexity in test API's used in this project
Complexity in performing the data validation scenarios as the values are constantly changing for API's because this is open API project and many users use and change data, so due to changes in the values, test cases fails. If we know the exact values inserted / configured in a project then we can execute various data validation tests to confirm values.

# Created CI/CD Pipeline and generated html report
Commited code on gitlab using commands: git clone, git commit -m "commited message", git add , git status, git push, git pull
Created gitlab-ci.yml file for generating CI/CD pipeline.
CI/CD pipeline will get trigger automatically after the commit and have made three stages in pipeline : build, test (this stage will create public folder and moves the reports inside it)

# See serenity HTML reports
Goto CI/CD - pipelines -> stage test (click) -> report-job (click) -> click Browse (right side) -> Public (click) ->
search file index.html (click and we can see reports), else we can also download the reports and see (that will
download public folder, so we can search index.html inside it
